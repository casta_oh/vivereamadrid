'use strict';

/* Services */

var vivereamadridServices = angular.module('vivereamadridServices', ['ngResource']);

vivereamadridServices.factory('Phone', ['$resource',
  function($resource){
    return $resource('phones/:phoneId.json', {}, {
      query: {method:'GET', params:{phoneId:'phones'}, isArray:true}
    });
  }]);

vivereamadridServices.factory('Post', ['$http', '$q', '$routeParams', function($http, $q, $routeParams) {

        return ({getAllPosts: getAllPosts,
                 getPostById: getPostById});

        function getAllPosts() {
            var page = 1;
            if ($routeParams.pageId != null) {
                page = $routeParams.pageId;
            }
            console.log('Obteniendo los posts de la pagina: ' + page);
            var request = $http({
                method: "get",
                url: "http://www.vivereamadrid.it/api/get_posts/",
                params: { page: page}
            });
            return( request.then( function(response) {
                console.log("Respuesta satisfactoria de getAllPosts. Numero de posts: " + response.data.posts.length);
                console.log("x-wp-total: " + response.data.count);
                console.log("x-wp-totalpages: " + response.data.pages);
                return( response );
            }, handleError ) );
        };

        function getPostById(postId) {
            console.log("Obteniendo el post con el id: " + postId);
            var request = $http({
                method: 'GET',
                url: 'http://www.vivereamadrid.it/api/get_post/?post_id=' + postId
            });
            return( request.then(function(response) {
                    console.log('Exito obteniendo el post');
                    return (response);
            }, handleError) );

        };


        function handleError( response ) {
            // The API response from the server should be returned in a
            // nomralized format. However, if the request was not handled by the
            // server (or what not handles properly - ex. server error), then we
            // may have to normalize it on our end, as best we can.
            if (
                ! angular.isObject( response.data ) ||
                ! response.data.message
                ) {
            return( $q.reject( "An unknown error occurred." ) );

            }

            // Otherwise, use expected error message.
            return( $q.reject( response.data.message ) );

        }

    }]);

vivereamadridServices.factory('Category', ['$http', '$q', function($http, $q) {

    return ({
        getCategories: getCategories,
        getPostsByCategory: getPostsByCategory
    });

    function getCategories() {
        console.log('Llamando a VivereAMadrid para obtener el listado de categorias');
        //
        var request = $http({
            method: "get",
            url: "http://www.vivereamadrid.it/api/get_category_index/",
            cache: true
        });
        return( request.then( function(response) {
            console.log("Respuesta satisfactoria de getCategories. Numero de categorias: " + response.data.categories.length);
            return( response );
        }, handleError ) );

    };

    function getPostsByCategory(categoryId) {

        console.log("Recuperando los posts para la categoria: " + categoryId);

        var request = $http({
            method: "get",
            url: "http://www.vivereamadrid.it/api/get_category_posts/?category_id=" + categoryId
        });
        return( request.then( function(response) {
            console.log("Respuesta satisfactoria de getPostsByCategory. Numero de posts de la categoria: " + response.data.posts.length);
            return( response );
        }, handleError ) );

    }

    function handleError( response ) {
        // The API response from the server should be returned in a
        // nomralized format. However, if the request was not handled by the
        // server (or what not handles properly - ex. server error), then we
        // may have to normalize it on our end, as best we can.
        if (
            ! angular.isObject( response.data ) ||
            ! response.data.message
            ) {
            return( $q.reject( "An unknown error occurred." ) );

        }

        // Otherwise, use expected error message.
        return( $q.reject( response.data.message ) );

    }

}]);