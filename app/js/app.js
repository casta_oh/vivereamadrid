'use strict';

/* App Module */

var vivereamadridApp = angular.module('vivereamadridApp', [
  'ngRoute',
  'vivereamadridAnimations',
  'vivereamadridControllers',
  'vivereamadridDirectives',
  'vivereamadridFilters',
  'truncate',
  'vivereamadridServices',
  'angulike'
]).run([
    '$rootScope', function ($rootScope) {
        $rootScope.facebookAppId = '[YOUR API KEY FACEBOOK]'; // set your facebook app id here
    }
]);

vivereamadridApp.config(['$routeProvider',
  function($routeProvider) {
    $routeProvider.

      when('/posts', {
            templateUrl: 'partials/post-list.html',
            controller: 'PostsCtrl'
      }).
      when('/post/:postId', {
          templateUrl: 'partials/post-detail.html',
          controller: 'PostCtrl'
      }).
      when('/posts-by-category/:categoryId', {
        templateUrl: 'partials/post-list.html',
        controller: 'PostsByCategoryCtrl'
      }).
      when('/posts/page/:pageId', {
            templateUrl: 'partials/post-list.html',
            controller: 'PostsCtrl'
        }).
      when('/main', {
            templateUrl: 'partials/main.html',
            controller: 'MainCtrl'
      }).
      when('/follow', {
            templateUrl: 'partials/follow.html',
            controller: 'MainCtrl'
      }).
      otherwise({
        redirectTo: '/main'
      });
  }]);

