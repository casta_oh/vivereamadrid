'use strict';

/* Controllers */

var vivereamadridControllers = angular.module('vivereamadridControllers', ['ngSanitize']);

vivereamadridControllers.controller('PostsCtrl', ['$scope', 'Post', 'Category', '$http', '$sce', '$routeParams',
  function($scope, Post, Category, $http, $sce, $routeParams) {

      $scope.page = 1;
      if ($routeParams.pageId != null) {
          $scope.page = $routeParams.pageId;
      }

      Post.getAllPosts().then(
          function (response) {
              $scope.posts = response.data.posts;
              $scope.totalPosts = response.data.count;
              $scope.totalPages = response.data.pages;
          }
      )

      $scope.fblogin = function() {
          FB.getLoginStatus(function(response) {
              if (response.status === 'connected') {
                  console.log('Logged in.');
              }
              else {
                  FB.login();
              }
          });
      }

  }]);


vivereamadridControllers.controller('PostsByCategoryCtrl', ['$scope', 'Category', '$http', '$sce', '$routeParams',
    function($scope, Category, $http, $sce, $routeParams) {

        Category.getPostsByCategory($routeParams.categoryId).then(
            function(response) {
                $scope.posts = response.data.posts;
                $scope.category = response.data.category;
            }
        )

    }]);

vivereamadridControllers.controller('PostCtrl', ['$scope', 'Post', '$http', '$sce', '$routeParams',
    function($scope, Post, $http, $sce, $routeParams) {
        Post.getPostById($routeParams.postId).then(
            function(response) {
                $scope.posttext = $sce.trustAsHtml(response.data.post.content);
                $scope.comments = response.data.post.comments;

                $scope.post = response.data.post;
                if (response.data.previous_id != null) {
                    $scope.previous = {
                        id: response.data.previous_id,
                        title: response.data.previous_title
                    };
                }
                if (response.data.next_id != null) {
                    $scope.next = {
                        id: response.data.next_id,
                        title: response.data.next_title
                    };
                };
            }
        )

}]);

vivereamadridControllers.controller('CategoryCtrl', ['$scope', 'Category', '$http', '$sce', '$routeParams',
    function($scope, Category, $http, $sce, $routeParams) {

        Category.getCategories().then(
            function (response) {
                $scope.categories = response.data.categories;
            }
        )

    }]);

vivereamadridControllers.controller('MainCtrl', ['$scope', 'Post', 'Category', '$http', '$sce', '$routeParams',
    function($scope, Post, Category, $http, $sce, $routeParams) {

        Post.getAllPosts().then(
            function (response) {
                $scope.post = response.data.posts[0];

            }
        )

    }]);
